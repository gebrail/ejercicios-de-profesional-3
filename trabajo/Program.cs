﻿using System;
using System.Collections.Generic;

namespace trabajo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Seleccione una de las siguientes opciones : ");
            Console.WriteLine("coloque su opcion de letra en mayuscula");

            Console.WriteLine("A : Lo llevar al ejercicio de nivel 1 llamado diccionario");
            Console.WriteLine("B : Lo llevara al ejercicio de nivel 1 llamado Invertido ");
            Console.WriteLine("C :  Lo llevara al ejercicio de nivel 2 llamado Diamantes y arena ");
            Console.WriteLine("D :  Lo llevara al ejercicio de nivel 2 llamado Matriz Loca ");
            Console.WriteLine("E :  Lo llevara al ejercicio de nivel 3 llamado Hallowen 3");
            Console.WriteLine("//////////////////////////");

            String opcion = Console.ReadLine();


            switch (opcion)
            {
            case "A":
                    Program.diccionario();
            break;

            case "B":
                    Program.invertido();
                    break;
            case "C":
                    Program.Diamonds_and_Sand();
                    break;
            case "D":
                    Program.matriz_loca();
                    break;

            case "E":
                    Program.halloween();
                    break;

                default:
                    
                    Console.WriteLine("No ejecuto ninguna opción adios");

                    Console.WriteLine("//////////////////////////");

                    break;
            }



        }



        static void diccionario()
        {
            //nivel 1
            int casos, n;
            string a;

            casos = int.Parse(Console.ReadLine());


            while ((casos--) != 0)
            {
                List<string> pala = new List<string>();

                n = int.Parse(Console.ReadLine());
                while ((n--) != 0)
                {
                    a = Console.ReadLine();
                    pala.Add(a);
                }

                pala.Sort();

                foreach (string palabras in pala)
                {
                    Console.Write(palabras);
                    Console.Write("\n");
                }
                Console.Write("-\n");
            }

            Console.ReadKey();

        }


        static void invertido()
        {

            // nilve 1

            int n;

            n = int.Parse(Console.ReadLine());

            string a;

            string b = "";

            while ((n--) != 0)

            {
                a = Console.ReadLine();

                b = a + "\n" + b;

            }

            Console.WriteLine("//////////////");

            Console.Write(b);
            Console.ReadKey();

        }


        static void Diamonds_and_Sand()
        {
            Diamond.DiamondsandSand();
        }


        static void matriz_loca() 
        {
            int casos;
            casos = int.Parse(ConsoleInput.ReadToWhiteSpace(true));
            while ((casos--) != 0)
            {
                int a;
                int b;
                a = int.Parse(ConsoleInput.ReadToWhiteSpace(true));
                b = int.Parse(ConsoleInput.ReadToWhiteSpace(true));
                int[][] matriz = RectangularArrays.RectangularIntArray(a, b);
                for (int i = 0; i < a; i++)
                {
                    for (int j = 0; j < b; j++)
                    {
                        matriz[i][j] = int.Parse(ConsoleInput.ReadToWhiteSpace(true));

                        //regla 1
                        if (i == 0 || i % 2 == 0)
                        {
                            matriz[i][j]++;
                        }
                        //regla 2
                        if (j == 0 || j % 2 == 0)
                        {
                            matriz[i][j] += 2;
                        }
                        //regla 3
                        if (i % 2 == 1 && j % 2 == 1)
                        {
                            matriz[i][j] -= 3;
                        }
                    }
                }

                for (int i = 0; i < a; i++)
                {
                    bool r = false;
                    for (int j = 0; j < b; j++)
                    {
                        if (r)
                        {
                            Console.Write(" ");
                        }
                        r = true;
                        Console.Write(matriz[i][j]);
                    }
                    Console.Write("\n");
                }

            }


            Console.ReadKey();


        }


        static void halloween()
        {
             //nivel 3
            Halloween.correr();

        }
    }






}
internal static class ConsoleInput
{
    private static bool goodLastRead = false;
    public static bool LastReadWasGood
    {
        get
        {
            return goodLastRead;
        }
    }

    public static string ReadToWhiteSpace(bool skipLeadingWhiteSpace)
    {
        string input = "";

        char nextChar;
        while (char.IsWhiteSpace(nextChar = (char)System.Console.Read()))
        {
            //accumulate leading white space if skipLeadingWhiteSpace is false:
            if (!skipLeadingWhiteSpace)
                input += nextChar;
        }
        //the first non white space character:
        input += nextChar;

        //accumulate characters until white space is reached:
        while (!char.IsWhiteSpace(nextChar = (char)System.Console.Read()))
        {
            input += nextChar;
        }

        goodLastRead = input.Length > 0;
        return input;
    }

    public static string ScanfRead(string unwantedSequence = null, int maxFieldLength = -1)
    {
        string input = "";

        char nextChar;
        if (unwantedSequence != null)
        {
            nextChar = '\0';
            for (int charIndex = 0; charIndex < unwantedSequence.Length; charIndex++)
            {
                if (char.IsWhiteSpace(unwantedSequence[charIndex]))
                {
                    //ignore all subsequent white space:
                    while (char.IsWhiteSpace(nextChar = (char)System.Console.Read()))
                    {
                    }
                }
                else
                {
                    //ensure each character matches the expected character in the sequence:
                    nextChar = (char)System.Console.Read();
                    if (nextChar != unwantedSequence[charIndex])
                        return null;
                }
            }

            input = nextChar.ToString();
            if (maxFieldLength == 1)
                return input;
        }

        while (!char.IsWhiteSpace(nextChar = (char)System.Console.Read()))
        {
            input += nextChar;
            if (maxFieldLength == input.Length)
                return input;
        }

        return input;
    }
}

//Helper class added by C++ to C# Converter:

//----------------------------------------------------------------------------------------
//	Copyright © 2006 - 2019 Tangible Software Solutions, Inc.
//	This class can be used by anyone provided that the copyright notice remains intact.
//
//	This class includes methods to convert C++ rectangular arrays (jagged arrays
//	with inner arrays of the same length).
//----------------------------------------------------------------------------------------
internal static class RectangularArrays
{
    public static int[][] RectangularIntArray(int size1, int size2)
    {
        int[][] newArray = new int[size1][];
        for (int array1 = 0; array1 < size1; array1++)
        {
            newArray[array1] = new int[size2];
        }

        return newArray;
    }
}
